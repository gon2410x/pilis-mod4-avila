import { Router } from "express";
import {getUsers, getUser, updateUser, deleteUser } from "../controllers/user.controller";
import {signIn, signUp, refresh } from '../controllers/user.controller'
import passport from 'passport'

const router = Router();

router.get("/users", passport.authenticate('jwt', { session: false }), getUsers);
router.get("/users/:id", passport.authenticate('jwt', { session: false }), getUser);
router.put("/users/:id", passport.authenticate('jwt', { session: false }), updateUser);
router.delete("/users/:id", passport.authenticate('jwt', { session: false }),deleteUser);

//Agregar para jwt
router.post('/signup', signUp);
router.post('/signin', signIn);
router.post('/token', refresh);

export default router;